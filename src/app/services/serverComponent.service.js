export class ServerComponentService {
  constructor () {
    'ngInject';
  }

  get() {
    return [{
      name: 'server1',
      x: 100,
      y: 200
    }, {
      name: 'server2',
      x: 300,
      y: 200
    }];
  }
}
