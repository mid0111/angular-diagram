export class MainController {
  constructor (ServerComponent) {
    'ngInject';

    this.ServerComponent = ServerComponent;
    this.servers = [];
    this.load();
  }

  load() {
    this.servers = this.ServerComponent.get();
  }
}
