describe('controllers', () => {
  let vm;

  beforeEach(angular.mock.module('angularDiagram'));

  beforeEach(inject(($controller, ServerComponent) => {
    spyOn(ServerComponent, 'get').and.returnValue([{
      name: 'testServer',
      x: '100',
      y: '500'
    }]);

    vm = $controller('MainController', {
      ServerComponent: ServerComponent
    });
  }));

  it('should have a server', () => {
    expect(vm.servers).toEqual([{
      name: 'testServer',
      x: '100',
      y: '500'
    }]);
  });
});
